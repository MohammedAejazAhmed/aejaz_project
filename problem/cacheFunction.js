function cb(value) {
    console.log("New number has been pushed")
}

function cacheFunction1(cb) {

    function invoke() {

        let array = [];
        let number = 78757778;
        var readline = require('readline');

        while (true) {

            let bool = true;

            const prompt = require('prompt-sync')();
            const number = parseInt(prompt('Enter number:'));
            for (let i = 0; i < array.length; i++) {
                if (number === array[i]) {
                    bool = false;
                    break;
                }
            }

            if (bool == true) {
                array.push(number);
                cb();
            } else if (bool == false) {
                console.log(array);
                break;
            }
        }
    }
    return (invoke);
}

module.exports = {
    cacheFunction1,
    cb
}