function cb() {
    console.log("Hello World");
}

function limitFunctionCallCount1(cb, n) {
    let count =0;
    
    return function callBack(){
        count++;
        if(count<=n){
            cb();
        }
    }
}
const funcTest = limitFunctionCallCount1(cb,4);

module.exports = {

    cb,
    limitFunctionCallCount1,
    funcTest
}
