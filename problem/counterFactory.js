function counterFactory1() {

    return {
        increment: function (number) {
            function inc() {
                number++;
            }
            inc();
            return number;
        },

        decrement: function (number) {
            function dec() {
                number--;
            }
            dec();
            return number;
        }
    }
}

module.exports = {

    counterFactory1

}