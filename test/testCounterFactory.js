const file = require('./../problem/counterFactory');

const increasedValue = file.counterFactory1().increment(4);
const decreasedValue = file.counterFactory1().decrement(4);

//increment will return the increasedValue and decreasedValue contains 5 and 3 you can console it
console.log(increasedValue + " " + decreasedValue);